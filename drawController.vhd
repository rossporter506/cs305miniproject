LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

entity drawController is
	port(ball_on, paddle_on, text_on, menu_on, menu_text, dark : in std_logic;
			red, green, blue : out std_logic );
end entity;

architecture beh of drawController is 
	--constants for regular colour theme
	constant backgroundColor : std_logic_vector (2 downto 0) := "111"; --white
	constant ballColor : std_logic_vector (2 downto 0) := "100"; --red
	constant paddleColor : std_logic_vector (2 downto 0) := "001"; -- blue
	constant textColor : std_logic_vector (2 downto 0) := "000"; -- black
	
	-- dark theme constants. I could probably have just 'not'ed the regular theme, 
	---but this gives us more flexibility in case a colour doesn't really work
	constant darkBackgroundColor : std_logic_vector (2 downto 0) := "000"; -- black
	constant darkBallColor : std_logic_vector (2 downto 0) := "011"; --cyan
	constant darkPaddleColor : std_logic_vector (2 downto 0) := "110"; --magenta
	constant darkTextColor : std_logic_vector (2 downto 0) := "111"; --white
begin
	---Things are drawn with the following priority:
	---Menu text displays over everything
	---The Main menu renders over everything else
	---in-game text renders over all other game elements
	---The ball has the next priority
	---Then the paddle is drawn
	---Background is drawn if nothing else is.
	process (text_on, ball_on, paddle_on, menu_text, menu_on)
	begin
		if menu_text = '1' then
			if (dark = '1') then
				red <= darkTextColor(0);
				green <= darkTextColor(1);
				blue <= darkTextColor(2);
			else
				red <= textColor(0);
				green <= textColor(1);
				blue <= textColor(2);
			end if;
		elsif menu_on = '1' then
			if (dark = '1') then
				red <= darkBackgroundColor(0);
				green <= darkBackgroundColor(1);
				blue <= darkBackgroundColor(2);
			else
				red <= backgroundColor(0);
				green <= backgroundColor(1);
				blue <= backgroundColor(2);
			end if;
		elsif (text_on = '1') then
			if (dark = '1') then
				red <= darkTextColor(0);
				green <= darkTextColor(1);
				blue <= darkTextColor(2);
			else
				red <= TextColor(0);
				green <= TextColor(1);
				blue <= TextColor(2);
			end if;
		elsif (ball_on = '1') then
			if (dark = '1') then
				red <= darkBallColor(0);
				green <= darkBallColor(1);
				blue <= darkBallColor(2);
			else
				red <= ballColor(0);
				green <= ballColor(1);
				blue <= ballColor(2);
			end if;
		elsif (paddle_on = '1') then
			if (dark = '1') then
				red <= darkPaddleColor(0);
				green <= darkPaddleColor(1);
				blue <= darkPaddleColor(2);
			else
				red <= paddleColor(0);
				green <= paddleColor(1);
				blue <= paddleColor(2);
			end if;
		else
			if (dark = '1') then
				red <= darkBackgroundColor(0);
				green <= darkBackgroundColor(1);
				blue <= darkBackgroundColor(2);
			else
				red <= backgroundColor(0);
				green <= backgroundColor(1);
				blue <= backgroundColor(2);
			end if;
		end if;
	end process;
end architecture beh;