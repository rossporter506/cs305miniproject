LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
-- Encodes Binary Coded Decimal into Seven Seg outputs
ENTITY sevenSegEncoder IS
   PORT( clock_25Mhz, reset, enable : IN std_logic;
         
			inOnes : in std_logic_vector (3 downto 0);
			inTens : in std_logic_vector (3 downto 0);
			inHundreds : in std_logic_vector (3 downto 0);
			inThousands : in std_logic_vector (3 downto 0);
			
			outOnes : out std_logic_vector (6 downto 0);
			outTens : out std_logic_vector (6 downto 0);
			outHundreds : out std_logic_vector (6 downto 0);
			outThousands : out std_logic_vector (6 downto 0));
END sevenSegEncoder;

architecture beh of sevenSegEncoder is
function sevenSegEncode ( numIn : in std_logic_vector (3 downto 0)) return std_logic_vector is
		variable numOut : std_logic_vector (6 downto 0);
	begin
		case numIn is
			when "0000" => numOut := "1000000"; -- 0
			when "0001" => numOut := "1111001"; -- 1
			when "0010" => numOut := "0100100"; -- 2
			when "0011" => numOut := "0110000"; -- 3
			when "0100" => numOut := "0011001"; -- 4
			when "0101" => numOut := "0010010"; -- 5
			when "0110" => numOut := "0000010"; -- 6
			when "0111" => numOut := "1111000"; -- 7
			when "1000" => numOut := "0000000"; -- 8
			when "1001" => numOut := "0010000"; -- 9
			when "1010" => numOut := "0001000"; -- A
			when "1011" => numOut := "0000011"; -- B
			when "1100" => numOut := "1000110"; -- C
			when "1101" => numOut := "0100001"; -- D
			when "1110" => numOut := "0001110"; -- E
			when "1111" => numOut := "0111111"; -- F
		end case;
	return numOut;
	end function;
begin
	process (clock_25Mhz)
	begin
		if rising_edge(clock_25Mhz) then
			outOnes <= sevenSegEncode(inOnes);
			outTens <= sevenSegEncode(inTens);
			outHundreds <= sevenSegEncode(inHundreds);
			outThousands <= sevenSegEncode(inThousands);
		end if;
	end process;
end architecture beh;