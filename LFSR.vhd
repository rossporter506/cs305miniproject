LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;

-- should be a maximal 8-register single bit LFSR
entity LFSR is
	port(	clk, enable, reset : in std_logic;
		seedBus : in std_logic_vector (7 downto 0); -- this is the value that will be reset to if reset goes high. Needs to be non-zero.
		qOut : out std_logic);
end entity;

architecture beh of LFSR is 
	signal reg : std_logic_vector (7 downto 0) := CONV_STD_LOGIC_VECTOR(1,8);
begin
	process (clk)
	begin
		if rising_edge(clk) then 
			if (reset = '1') then
				reg <= seedBus;
			else -- set up register chain
				for n in 0 to 6 loop
					if (n < 3 or n > 5) then -- no tap
						reg(n+1) <= reg(n);
					else -- taps on 4,5,6
						reg(n+1) <= reg(n) xor reg(7);
					end if;
				end loop;
				reg(0) <= reg(7);
			end if;
		end if;
		qOut <= reg(7);
	end process;
end architecture beh;