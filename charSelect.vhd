library IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.STD_LOGIC_arith.all;
USE IEEE.STD_LOGIC_unsigned.all;

entity charSelect is
	port(	clk, isEnabled : in std_logic;
			pixelRow, pixelColumn : in std_logic_vector (9 downto 0);
			characterAddress : out std_logic_vector (5 downto 0);
			scoreOnes, scoreTens, scoreHundreds, scoreThousands : in std_logic_vector (3 downto 0);
			game_mode: in std_logic_vector (1 downto 0);
			menu_on : out std_logic;
			level : in std_logic_vector (1 downto 0));
end entity;

architecture beh of charSelect is
--This function takes the score number and finds the memory address to display it
function encodeNumber ( numIn : in std_logic_vector (3 downto 0)) return std_logic_vector is
		variable numOut : std_logic_vector (5 downto 0);
	begin
		case numIn is
			when "0000" => numOut := conv_std_logic_vector(48,6); -- 0
			when "0001" => numOut := conv_std_logic_vector(49,6); -- 1
			when "0010" => numOut := conv_std_logic_vector(50,6); -- 2
			when "0011" => numOut := conv_std_logic_vector(51,6); -- 3
			when "0100" => numOut := conv_std_logic_vector(52,6); -- 4
			when "0101" => numOut := conv_std_logic_vector(53,6); -- 5
			when "0110" => numOut := conv_std_logic_vector(54,6); -- 6
			when "0111" => numOut := conv_std_logic_vector(55,6); -- 7
			when "1000" => numOut := conv_std_logic_vector(56,6); -- 8
			when "1001" => numOut := conv_std_logic_vector(57,6); -- 9
			when others => numOut := conv_std_logic_vector(32,6);
		end case;
	return numOut;
	end function;

begin
	process (clk)
	constant hbits : integer := 5;
	constant wbits : integer := 5;
	constant w : integer := 2**wbits; -- width of character
	constant h : integer := 2**hbits; -- height of character
	
	constant mw : integer := 7*w; --middle of screen
	constant mh : integer := 7*h;
	
	constant menu_h : integer := mh - 100; -- where we write 'PROJECT PONG' at the main menu
	constant menu_w : integer := mw - w*3;
	
	begin
		if rising_edge(clk) then
			if (game_mode = "00") then -- if we're at the main menu
				menu_on <= '1';
				---draw 'PROJECT PONG'
				if (pixelRow > conv_std_logic_vector(menu_h,10) and pixelRow < conv_std_logic_vector(menu_h+h,10)) then --check if we're in the right rows to draw characters
					if ((pixelColumn((wbits-1) downto 0) = conv_std_logic_vector(0,(wbits)) )) then -- check to see if we're in the right column to start a new character
						if (pixelColumn = conv_std_logic_vector(menu_w+0*w,10)) then
							characterAddress <= conv_std_logic_vector(16,6); 			--P
						elsif (pixelColumn = conv_std_logic_vector(menu_w+1*w,10)) then
							characterAddress <= conv_std_logic_vector(18,6); 			--R
						elsif (pixelColumn = conv_std_logic_vector(menu_w+2*w,10)) then
							characterAddress <= conv_std_logic_vector(15,6); 			--O
						elsif (pixelColumn = conv_std_logic_vector(menu_w+3*w,10)) then
							characterAddress <= conv_std_logic_vector(10,6); 			--J
						elsif (pixelColumn = conv_std_logic_vector(menu_w+4*w,10)) then
							characterAddress <= conv_std_logic_vector(5,6); 			--E
						elsif (pixelColumn = conv_std_logic_vector(menu_w+5*w,10)) then
							characterAddress <= conv_std_logic_vector(3,6); 			--C
						elsif (pixelColumn = conv_std_logic_vector(menu_w+6*w,10)) then
							characterAddress <= conv_std_logic_vector(20,6); 			--T
						elsif (pixelColumn = conv_std_logic_vector(menu_w+7*w,10)) then
							characterAddress <= conv_std_logic_vector(32,6); 			--' '
						elsif (pixelColumn = conv_std_logic_vector(menu_w+8*w,10)) then
							characterAddress <= conv_std_logic_vector(16,6); 			--P
						elsif (pixelColumn = conv_std_logic_vector(menu_w+9*w,10)) then
							characterAddress <= conv_std_logic_vector(15,6); 			--O
						elsif (pixelColumn = conv_std_logic_vector(menu_w+10*w,10)) then
							characterAddress <= conv_std_logic_vector(14,6); 			--N
						elsif (pixelColumn = conv_std_logic_vector(menu_w+11*w,10)) then
							characterAddress <= conv_std_logic_vector(7,6); 			--G
						else
							characterAddress <= conv_std_logic_vector(32,6); -- ' '
						end if;
					end if;
				elsif (pixelRow > conv_std_logic_vector(menu_h+4*h,10) and pixelRow < conv_std_logic_vector(menu_h+5*h,10)) then --draw 'play -> PB1'
					if ((pixelColumn((wbits-1) downto 0) = conv_std_logic_vector(0,(wbits)) )) then -- check to see if it's time to draw a new character
						if (pixelColumn = conv_std_logic_vector(menu_w+0*w,10)) then
							characterAddress <= conv_std_logic_vector(16,6); 			-- P
						elsif (pixelColumn = conv_std_logic_vector(menu_w+1*w,10)) then
							characterAddress <= conv_std_logic_vector(12,6); 			-- L
						elsif (pixelColumn = conv_std_logic_vector(menu_w+2*w,10)) then
							characterAddress <= conv_std_logic_vector(1,6); 			-- A
						elsif (pixelColumn = conv_std_logic_vector(menu_w+3*w,10)) then
							characterAddress <= conv_std_logic_vector(25,6); 			-- Y
						elsif (pixelColumn = conv_std_logic_vector(menu_w+4*w,10)) then
							characterAddress <= conv_std_logic_vector(32,6); 			--' '
						elsif (pixelColumn = conv_std_logic_vector(menu_w+5*w,10)) then
							characterAddress <= conv_std_logic_vector(32,6); 			--' '
						elsif (pixelColumn = conv_std_logic_vector(menu_w+6*w,10)) then
							characterAddress <= conv_std_logic_vector(31,6); 			--'->'
						elsif (pixelColumn = conv_std_logic_vector(menu_w+7*w,10)) then
							characterAddress <= conv_std_logic_vector(32,6); 			--' '
						elsif (pixelColumn = conv_std_logic_vector(menu_w+8*w,10)) then
							characterAddress <= conv_std_logic_vector(16,6); 			-- P
						elsif (pixelColumn = conv_std_logic_vector(menu_w+9*w,10)) then
							characterAddress <= conv_std_logic_vector(2,6); 			-- B
						elsif (pixelColumn = conv_std_logic_vector(menu_w+10*w,10)) then
							characterAddress <= conv_std_logic_vector(49,6); 			-- 1
						else
							characterAddress <= conv_std_logic_vector(32,6); -- ' '
						end if;
					end if;
				elsif (pixelRow > conv_std_logic_vector(menu_h+5*h,10) and pixelRow < conv_std_logic_vector(menu_h+6*h,10)) then --draw 'train -> PB2'
					if ((pixelColumn((wbits-1) downto 0) = conv_std_logic_vector(0,(wbits)) )) then -- check to see if it's time to draw a new character
						if (pixelColumn = conv_std_logic_vector(menu_w+0*w,10)) then
							characterAddress <= conv_std_logic_vector(20,6); 			-- T
						elsif (pixelColumn = conv_std_logic_vector(menu_w+1*w,10)) then
							characterAddress <= conv_std_logic_vector(18,6); 			-- R
						elsif (pixelColumn = conv_std_logic_vector(menu_w+2*w,10)) then
							characterAddress <= conv_std_logic_vector(1,6); 			-- A
						elsif (pixelColumn = conv_std_logic_vector(menu_w+3*w,10)) then
							characterAddress <= conv_std_logic_vector(9,6); 			-- I
						elsif (pixelColumn = conv_std_logic_vector(menu_w+4*w,10)) then
							characterAddress <= conv_std_logic_vector(14,6); 			-- N
						elsif (pixelColumn = conv_std_logic_vector(menu_w+5*w,10)) then
							characterAddress <= conv_std_logic_vector(32,6); 			--' '
						elsif (pixelColumn = conv_std_logic_vector(menu_w+6*w,10)) then
							characterAddress <= conv_std_logic_vector(31,6); 			--'->'
						elsif (pixelColumn = conv_std_logic_vector(menu_w+7*w,10)) then
							characterAddress <= conv_std_logic_vector(32,6); 			--' '
						elsif (pixelColumn = conv_std_logic_vector(menu_w+8*w,10)) then
							characterAddress <= conv_std_logic_vector(16,6); 			-- P
						elsif (pixelColumn = conv_std_logic_vector(menu_w+9*w,10)) then
							characterAddress <= conv_std_logic_vector(2,6); 			-- B
						elsif (pixelColumn = conv_std_logic_vector(menu_w+10*w,10)) then
							characterAddress <= conv_std_logic_vector(50,6); 			-- 2
						else
							characterAddress <= conv_std_logic_vector(32,6);
						end if;
					end if;
				end if;
			else 
				menu_on <= '0';
				if (isEnabled = '0') then -- draw 'PAUSED' in the middle of the screen
					if (pixelRow > conv_std_logic_vector(mh,10) and pixelRow < conv_std_logic_vector(mh+h,10)) then --draw in the middle rows only
						if ((pixelColumn((wbits-1) downto 0) = conv_std_logic_vector(0,(wbits)) )) then -- check to see if it's time to draw a new character
							if (pixelColumn = conv_std_logic_vector(mw+0*w,10)) then
								characterAddress <= conv_std_logic_vector(16,6); 		--P
							elsif (pixelColumn = conv_std_logic_vector(mw+1*w,10)) then
								characterAddress <= conv_std_logic_vector(1,6); 		--A
							elsif (pixelColumn = conv_std_logic_vector(mw+2*w,10)) then
								characterAddress <= conv_std_logic_vector(21,6); 		--U
							elsif (pixelColumn = conv_std_logic_vector(mw+3*w,10)) then
								characterAddress <= conv_std_logic_vector(19,6); 		--S
							elsif (pixelColumn = conv_std_logic_vector(mw+4*w,10)) then
								characterAddress <= conv_std_logic_vector(5,6); 		--E
							elsif (pixelColumn = conv_std_logic_vector(mw+5*w,10)) then
								characterAddress <= conv_std_logic_vector(4,6); 		--D
							else
								characterAddress <= conv_std_logic_vector(32,6);
							end if;
						end if;
					end if;
				end if;
				
				--write score in the top left
				if (pixelRow < conv_std_logic_vector(h,10)) then -- only draw on the first row
					if (pixelColumn((wbits-1) downto 0) = conv_std_logic_vector(0,(wbits)) ) then 
						if (pixelColumn = conv_std_logic_vector(0,10)) then 
							characterAddress <= conv_std_logic_vector(19,6);	-- S
						elsif (pixelColumn = conv_std_logic_vector(1*w,10)) then
							characterAddress <= conv_std_logic_vector(3,6);		-- C
						elsif (pixelColumn = conv_std_logic_vector(2*w,10)) then
							characterAddress <= conv_std_logic_vector(15,6);	-- O
						elsif (pixelColumn = conv_std_logic_vector(3*w,10)) then
							characterAddress <= conv_std_logic_vector(18,6);	-- R
						elsif (pixelColumn = conv_std_logic_vector(4*w,10)) then
							characterAddress <= conv_std_logic_vector(5,6);		-- E
						elsif (pixelColumn = conv_std_logic_vector(5*w,10)) then
							characterAddress <= conv_std_logic_vector(32,6);	--' '
						elsif (pixelColumn = conv_std_logic_vector(6*w,10)) then
							characterAddress <= encodeNumber(scoreThousands);	-- X
						elsif (pixelColumn = conv_std_logic_vector(7*w,10)) then
							characterAddress <= encodeNumber(scoreHundreds);	-- X
						elsif (pixelColumn = conv_std_logic_vector(8*w,10)) then
							characterAddress <= encodeNumber(scoreTens);		-- X
						elsif (pixelColumn = conv_std_logic_vector(9*w,10)) then
							characterAddress <= encodeNumber(scoreOnes);		-- X
						
						-- Draw the level in the right hand corner
						elsif ((pixelColumn = conv_std_logic_vector(17*w,10))) then -- L
							characterAddress <= conv_std_logic_vector(12,6);
						elsif ((pixelColumn = conv_std_logic_vector(18*w,10))) then -- X
							characterAddress <= encodeNumber(("00" & level) +'1');
						
						else														--' '
							characterAddress <= conv_std_logic_vector(32,6);
						end if;
					end if;
				end if;
			end if;
		end if;
	end process;
end architecture beh;