library ieee;
use ieee.std_logic_1164.all;

entity secondClock is 
	port (
		clk : in std_logic;
		clk_out : out std_logic
	);
end secondClock;

-- A simple clock divider that turns a 25MHz clock into a roughly 1Hz clock
architecture behaviour of secondClock is 
signal count : integer := 0;
signal q_clk_out : std_logic;
begin
	process (clk) begin
		if rising_edge(clk) then
			if (count = 12499999) then
				q_clk_out <= not q_clk_out;
				count <= 0;
			else
				count <= count + 1;
			end if;
		end if;
	end process;
	clk_out <= q_clk_out;
end architecture;