library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity scoreController is
	port( v_sync, caught1, caught2, miss1, miss2 : in std_logic;
			game_mode		: in std_logic_vector(1 downto 0);
			scoreOnes, scoreTens, scoreHundreds, scoreThousands	: out std_logic_vector(3 downto 0)
	);
end entity scoreController;

architecture a1 of scoreController is
signal sones, stens, shundreds, sthousands : std_logic_vector(3 downto 0);
signal prevMiss1, prevMiss2 : std_logic;
begin

process(v_sync, sones, stens, shundreds, sthousands)

begin 
	if(rising_edge(v_sync)) then
		prevMiss1 <= miss1; -- Update strobe info
		prevMiss2 <= miss2;
		if (game_mode = "00") then -- reset clock at main menu
			sones <= conv_std_logic_vector(0, 4);
			stens <= conv_std_logic_vector(0, 4);
			shundreds <= conv_std_logic_vector(0, 4);
			sthousands <= conv_std_logic_vector(0, 4);
		else -- if playing
			if(caught1 = '1' or caught2 = '1') then	-- If either ball was caught
				if (caught1 = '1' and caught2 = '1') then -- If both balls are caught at once, increment twice
					if (shundreds >= conv_std_logic_vector(8, 4)) then -- If we overflow into hundreds
						sthousands <= sthousands + '1'; -- increment the hundreds column
						if (shundreds = conv_std_logic_vector(9,4)) then -- set tens column to correct value after carrying the one
							shundreds <= conv_std_logic_vector(1, 4);
						else
							shundreds <= conv_std_logic_vector(0, 4);
						end if;
					else -- if it won't roll over, we can just add two to the tens column
						shundreds <= shundreds + "10";
					end if;
				else -- If only one ball was caught, increment score once
					if (shundreds = conv_std_logic_vector(9, 4)) then -- if overflow into hundreds
						sthousands <= sthousands + '1'; -- increment hundreds
						shundreds <= "0000"; -- reset tens
					else -- if no overflow, increment normally
						shundreds <= shundreds + '1'; 
					end if;
				end if;
			elsif ((miss1 = '1') and (prevMiss1 = '0')) or ((miss2 = '1') and (prevMiss2 = '0')) then --If a ball has missed
				if ((miss1 = '1') and (prevMiss1 = '0')) and ((miss2 = '1') and (prevMiss2 = '0')) then -- If both balls miss at once, decrement twice
					if (stens >= conv_std_logic_vector(2, 4)) then -- if there's enough, take from tens
						stens <= stens - "10";
					else
						if (shundreds /= conv_std_logic_vector(0, 4)) then -- otherwise we need to take from hundreds column
							shundreds <= shundreds - '1';
							if (stens = conv_std_logic_vector(1,4)) then
								stens <= conv_std_logic_vector(9, 4);
							else
								stens <= conv_std_logic_vector(8, 4);
							end if;
						else -- if we don't have any hundreds
							if (sthousands /= conv_std_logic_vector(0, 4)) then -- otherwise we need to take from thousands column
								sthousands <= sthousands - '1';
								shundreds <= conv_std_logic_vector(9, 4);
								if (stens = conv_std_logic_vector(1,4)) then
									stens <= conv_std_logic_vector(9, 4);
								else
									stens <= conv_std_logic_vector(8, 4);
								end if;
							else
								null;
							end if;
						end if;
					end if;

				else -- If only one ball was missed
					if (stens /= conv_std_logic_vector(0, 4)) then -- if there's enough, take from tens
						stens <= stens - '1';
					else -- if we don't have enough in the tens
						if (shundreds /= conv_std_logic_vector(0, 4)) then -- otherwise we need to take from hundreds column
							shundreds <= shundreds - '1';
							stens <= conv_std_logic_vector(9, 4);
						else -- if we don't have enough in the hundreds
							if (sthousands /= conv_std_logic_vector(0, 4)) then -- otherwise we need to take from thousands column
								sthousands <= sthousands - '1';
								shundreds <= conv_std_logic_vector(9, 4);
								stens <= conv_std_logic_vector(9, 4);
							else
								null;
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
	scoreOnes <= sones;
	scoreTens <= stens;
	scoreHundreds <= shundreds;
	scoreThousands <= sthousands;
end process;
end architecture;