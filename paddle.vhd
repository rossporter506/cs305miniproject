-- Bouncing paddle Video 
--

			-- Bouncing paddle Video 
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_SIGNED.all;

ENTITY paddle IS
   PORT(	Clock, enable 				: IN std_logic;
			mouse_X_Pos 				: IN std_logic_vector (9 downto 0);
			pixel_column, pixel_row		: IN std_logic_vector (9 downto 0);
			vert_sync_int				: IN std_logic;
			level						: IN std_logic_vector (1 downto 0);
			paddle_on					: OUT std_logic;
			paddle_pos, paddle_size 	: OUT std_logic_vector (10 downto 0));
END paddle;

architecture behavior of paddle is

			-- Video Display Signals   
SIGNAL reset, v_paddle_on, Direction			: std_logic;
SIGNAL X_Size, Y_Size 							: std_logic_vector(9 DOWNTO 0);
SIGNAL Paddle_Y_motion, Paddle_x_motion 		: std_logic_vector(9 DOWNTO 0);
SIGNAL Paddle_Y_pos								: std_logic_vector (10 downto 0) := CONV_STD_LOGIC_VECTOR(467,11);
signal Paddle_X_pos								: std_logic_vector(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(320,11);
signal red,green,blue 							: std_logic;
signal Horiz_sync, Vert_sync					: std_logic;

BEGIN
-- Set paddle height
Y_Size <= CONV_STD_LOGIC_VECTOR(3,10);

-- Change paddle width  as level changes
X_Size <=	CONV_STD_LOGIC_VECTOR(80,10) when level = "00" else 
			CONV_STD_LOGIC_VECTOR(40,10) when level = "01" else
			CONV_STD_LOGIC_VECTOR(20,10);

paddle_on <= v_paddle_on;

RGB_Display: Process (Paddle_X_pos, Paddle_Y_pos, pixel_column, pixel_row, X_Size, Y_Size)
BEGIN
			-- Set v_paddle_on ='1' to display paddle
	IF ("00" & Paddle_X_pos <= '0' & pixel_column + X_Size) AND
	-- pad zeroes to make sure we're comparing positive numbers
	('0' & Paddle_X_pos + X_Size >= "00" & pixel_column) AND
	("00" & Paddle_Y_pos <= pixel_row + Y_Size) AND 
	(Paddle_Y_pos + Y_Size >= "00" & pixel_row ) THEN 
		v_paddle_on <= '1';
	ELSE
		v_paddle_on <= '0';
	END IF;
END process RGB_Display;

Move_Paddle: process
BEGIN
	-- Move paddle once every vertical sync
	WAIT UNTIL vert_sync_int'event and vert_sync_int = '1';
		if (enable = '1') then
			-- Compute next paddle position
			Paddle_X_pos <= '0' & mouse_X_Pos;
			
			-- Generate Output signals
			paddle_pos <= '0' & mouse_X_Pos;
			paddle_size <= '0' & X_Size;
		end if;
END process Move_Paddle;

END behavior;

