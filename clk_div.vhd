library ieee;
use ieee.std_logic_1164.all;

entity clk_div is 
	port (
		clk : in std_logic;
		clk_out : out std_logic
	);
end clk_div;
 -- divides a clock signal in twain
architecture behaviour of clk_div is 
signal counter : std_logic;
signal q_clk_out : std_logic;
begin
	process (clk) begin
		if rising_edge(clk) then
			q_clk_out <= not q_clk_out;
		end if;
	end process;
	clk_out <= q_clk_out;
end architecture;