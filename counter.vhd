library ieee;
use ieee.std_logic_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

entity counter is 
	port (clk, reset, enable : in std_logic;
			ones : out std_logic_vector (3 downto 0);
			tens : out std_logic_vector (3 downto 0);
			hundreds : out std_logic_vector (3 downto 0);
			thousands : out std_logic_vector (3 downto 0);
			done : out std_logic := '0');
end counter;

architecture behaviour of counter is 
signal v_ones : std_logic_vector (3 downto 0) := conv_std_logic_vector(0, 4);
signal v_tens : std_logic_vector (3 downto 0) := conv_std_logic_vector(5, 4);
signal v_hundreds : std_logic_vector (3 downto 0) := conv_std_logic_vector(0, 4);
signal v_thousands : std_logic_vector (3 downto 0) := conv_std_logic_vector(0, 4);
begin
	process (clk, v_ones, v_tens, v_hundreds, reset) begin
		if (reset = '1') then
			v_ones <= conv_std_logic_vector(0, 4);
			v_tens <= conv_std_logic_vector(5, 4);
			v_hundreds <= conv_std_logic_vector(0, 4);
			v_thousands <= conv_std_logic_vector(0, 4);
		elsif rising_edge(clk) then
			if (enable = '1') then -- start counting down
				if (v_ones = "0000") then -- we need to borrow from the tens
					if (v_tens = "0000") then -- we need to borrow from the hundreds
						if (v_hundreds = "0000") then -- we must be done. Reset and send done signal
							done <= '1';
							v_ones <= conv_std_logic_vector(0, 4);
							v_tens <= conv_std_logic_vector(4, 4);
							v_hundreds <= conv_std_logic_vector(0, 4);
							v_thousands <= conv_std_logic_vector(0, 4);
						else -- decrement hundreds and set the rest to '99'
							v_hundreds <= v_hundreds - '1';
							v_tens <= conv_std_logic_vector(9, 4);
							v_ones <= conv_std_logic_vector(9, 4);
						end if;
					else -- decrement the tens and set the ones to '9'
						v_tens <= v_tens - '1';
						v_ones <= conv_std_logic_vector(9, 4);
					end if;
				else -- just decrement the ones
					v_ones <= v_ones - '1';
				end if;
			else -- Since we disable the timer (timer doesn't count in main menu) when it reaches zero, this resets the done signal as you would expect
				done <= '0';
			end if;
		end if;
	end process;
	ones <= v_ones;
	tens <= v_tens;
	hundreds <= v_hundreds;
	thousands <= v_thousands;
end architecture;