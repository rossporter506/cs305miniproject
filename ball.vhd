-- Bouncing Ball Video 
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_SIGNED.all;


ENTITY ball IS
   PORT(SIGNAL Clk, enable, reset				: IN std_logic;
			signal pixel_column, pixel_row 		: IN std_LOGIC_VECTOR (9 downto 0);
			signal ball_on, caught, miss 			: out std_logic;
			signal paddle_position, paddle_size : in std_logic_vector (10 downto 0);
			signal vert_sync_int						: in std_logic;
			signal level 								: in std_logic_vector (1 downto 0));
END ball;

architecture behavior of ball is
		--Ball movement and control signals  
SIGNAL v_ball_on, Direction				: std_logic;
SIGNAL Size 									: std_logic_vector(9 DOWNTO 0);  
SIGNAL Ball_Y_motion, Ball_x_motion		: std_logic_vector(3 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(2,4);
SIGNAL Ball_Y_pos								: std_logic_vector (10 downto 0) := CONV_STD_LOGIC_VECTOR(-8,11);
signal Ball_X_pos								: std_logic_vector(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(320,11);
signal ballSpeed 								: std_logic_vector (3 downto 0) := CONV_STD_LOGIC_VECTOR(4,4);
signal ballSpawnPos							: std_LOGIC_VECTOR (8 downto 0);

component LFSR8bit is
	port(clk, enable, reset : in std_logic;
			output : out std_logic_vector (8 downto 0));
end component;

function speedLimit ( numIn : in std_logic_vector (3 downto 0)) return std_logic_vector is
		variable numOut : std_logic_vector (3 downto 0);
	begin
		case numIn is
			when "1000" => numOut := "1001";
			when others => numOut := numIn;
		end case;
	return numOut;
end function;
	
BEGIN           

LFSR8: LFSR8bit port map (clk => clk, enable => '1', reset => reset, output => ballSpawnPos);

Size <= CONV_STD_LOGIC_VECTOR(8,10);
ballSpeed <= 	CONV_STD_LOGIC_VECTOR(2,4) when (level = "00") else
					CONV_STD_LOGIC_VECTOR(4,4) when (level = "01") else
					CONV_STD_LOGIC_VECTOR(6,4) when (level = "10") else
					CONV_STD_LOGIC_VECTOR(2,4);
ball_on <= v_ball_on;

RGB_Display: Process (Ball_X_pos, Ball_Y_pos, pixel_column, pixel_row, Size)
BEGIN
			-- Set v_ball_on ='1' to display ball
 IF ("00" & Ball_X_pos <= '0' & pixel_column + Size) AND
 			-- compare positive numbers only
 	('0' & Ball_X_pos + Size >= "00" & pixel_column) AND
	
 	("00" & Ball_Y_pos <= pixel_row + Size) AND
 	(Ball_Y_pos + Size >= "00" & pixel_row ) THEN
 		v_ball_on <= '1';
 	ELSE
 		v_ball_on <= '0';
END IF;
END process RGB_Display;

Move_Ball: process
	variable ball_caught : std_logic;
	variable v_miss : std_logic;
BEGIN
			-- Move ball once every vertical sync to prevent screen tearing
	WAIT UNTIL vert_sync_int'event and vert_sync_int = '1';
		if (reset = '1') then
			-- when the game first starts up, this sets the (random) direction of the first ball
			if ballSpawnPos(2 downto 1) = "00" then
				Ball_X_Motion <= -conv_STD_LOGIC_VECTOR(3,4);
			elsif(ballSpawnPos(5) = '1') then
				Ball_X_Motion <= -(CONV_STD_LOGIC_VECTOR(0,2) & ballSpawnPos(1 downto 0));
			else
				Ball_X_Motion <= (CONV_STD_LOGIC_VECTOR(0,2) & ballSpawnPos(1 downto 0));
			end if;
			--also start in a random position
			Ball_X_Pos <= '0' & (('0' & ballSpawnPos) + ('0' & CONV_STD_LOGIC_VECTOR(50,9)));
			Ball_Y_Pos <= CONV_STD_LOGIC_VECTOR(-8,11);
			
		elsif (enable = '1') then -- if the game has started
			--check if the ball has been caught
			if (Ball_Y_Pos >= CONV_STD_LOGIC_VECTOR(456,10) 	--Check if ball is near paddle in y
				and Ball_Y_Pos <= CONV_STD_LOGIC_VECTOR(460,10) 
				and Ball_Y_motion > CONV_STD_LOGIC_VECTOR(0,10) --check if ball is going down
				and Ball_X_Pos >= paddle_position - paddle_size --check if ball is near paddle in x
				and Ball_X_Pos <= paddle_position + paddle_size) then
				ball_caught := '1';
			else 
				ball_caught := '0';
			end if;
			v_miss := '0'; -- reset the miss signal
			
			if (ball_caught = '0') then -- do regular movement
				-- Bounce off left or right of screen
				IF Ball_X_pos <= Size and (Ball_X_motion < CONV_STD_LOGIC_VECTOR(0,4)) THEN -- if ball is close to the left hand wall and is moving towards it
					Ball_X_motion <= -Ball_X_Motion; -- invert direction
				ELSIF (Ball_X_pos) >= CONV_STD_LOGIC_VECTOR(640,11) - ('0' & Size) and (Ball_X_motion > CONV_STD_LOGIC_VECTOR(0,4)) THEN -- if ball is close to the right hand wall and is moving towards it
					Ball_X_motion <= -Ball_X_Motion; -- invert direction
				END IF;
				
				-- Bounce off top or bottom of screen
				IF Ball_Y_pos <= Size THEN -- if ball is close the top wall
					Ball_Y_motion <= ballSpeed;
				ELSIF (Ball_Y_pos) >= CONV_STD_LOGIC_VECTOR(480,11) - ('0' & Size) THEN -- if ball is close the bottom wall
					Ball_Y_motion <= -ballSpeed;
					v_miss := '1'; -- set the miss flag
				END IF;
				
				--Update position
				Ball_X_pos <= Ball_X_pos + Ball_X_motion; -- Compute next ball X position
				Ball_Y_pos <= Ball_Y_pos + Ball_Y_motion; -- Compute next ball Y position
				
			else -- the ball has been caught and needs to be reset to spawn in a random position and random direction
				-- The following line introduces a bug where the ball would sometimes (very occasionally) fly through the left hand wall.
				--Ball_X_Pos <= "00" & ((ballSpawnPos) + (CONV_STD_LOGIC_VECTOR(50,9)));
				
				--This code has less randomness, but doesn't seem to have the bug
				case ballSpawnPos(2 downto 0) is
					when "000" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(100,11);
					when "001" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(100,11);
					when "010" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(200,11);
					when "011" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(200,11);
					when "100" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(300,11);
					when "101" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(300,11);
					when "110" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(400,11);
					when "111" => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(400,11);
					when others => Ball_X_Pos <= CONV_STD_LOGIC_VECTOR(600,11);
				end case;
				
				Ball_Y_Pos <= CONV_STD_LOGIC_VECTOR(-8,11);
				if level = "00" then
					if ballSpawnPos(0) = '1' then -- 50% chance to go left or right
						Ball_X_Motion <= "0010"; -- start going left
					else
						Ball_X_Motion <= "1110"; -- start going right
					end if;
				elsif level = "01" then -- 50% chance to go left or right
					if ballSpawnPos(0) = '1' then
						Ball_X_Motion <= "0100"; -- start going left
					else
						Ball_X_Motion <= "1100"; -- start going right
					end if;
				elsif level = "10" then -- 50% chance to go left or right
					if ballSpawnPos(0) = '1' then
						Ball_X_Motion <= "0111"; -- start going left
					else
						Ball_X_Motion <= "1001"; -- start going right
					end if;
				else
					Ball_X_Motion <= "0000"; -- If there's an edge case, move straight down so it's noticeable
				end if;
			end if;	
		end if;
	caught <= ball_caught;
	miss <= v_miss;
END process Move_Ball;

END behavior;

