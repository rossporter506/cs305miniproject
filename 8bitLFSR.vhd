LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;

-- combines several LFSRs to produce a 9-bit output (0 -> 511)
entity LFSR8Bit is
	port(	clk, enable, reset : in std_logic;
		output : out std_logic_vector (8 downto 0));
end entity LFSR8Bit;

architecture beh of LFSR8Bit is 
	component LFSR is 
		port(	clk, enable, reset : in std_logic;
			seedBus : in std_logic_vector (7 downto 0);
			qOut : out std_logic);
	end component;
begin
	LFSRs : for N in 0 to 8 generate 
	LFSRX: LFSR port map ( 	clk => clk, 
				reset => reset, 
				enable => enable,
				seedBus => CONV_STD_LOGIC_VECTOR(10*N+1,8), -- map each LFSR's initial seed to a "random" non-zero value so we don't get 9 LFSRs all outputting the same thing
				qOut => output(N) );
	end generate LFSRs;
end architecture beh;