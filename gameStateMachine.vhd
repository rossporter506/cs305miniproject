LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

entity gameStateMachine is
	port(	clk, v_sync, caught1, caught2, play, train, timesUp, returnToMenu : in std_logic;
			mode, level : out std_logic_vector (1 downto 0);
			enable, mode_changed : out std_logic);
end entity gameStateMachine;

architecture beh of gameStateMachine is
	type sys_state is (main_menu, regular, training);
	signal state : sys_state := main_menu;
	signal catch_counter : integer range 0 to 10 := 0;
	signal v_level : std_logic_vector (1 downto 0) := "00";
begin
	nextState: process (v_sync)
	begin
		if rising_edge(v_sync) then -- next state logic
			case state is 
				when main_menu => 
					if (play = '0') then
						state <= regular;
						mode_changed <= '1';
					elsif (train ='0') then
						state <= training;
						mode_changed <= '1';
					else 
						state <= main_menu;
						mode_changed <= '0';
					end if;
				when regular =>
					if (timesUp = '1') or (v_level >= "11") or (returnToMenu = '0') then 
						state <= main_menu;
						mode_changed <= '1';
					else
						state <= regular;
						mode_changed <= '0';
					end if;
				when training =>
					if (timesUp = '1') or (v_level >= "01") or (returnToMenu = '0') then
						state <= main_menu;
						mode_changed <= '1';
					else
						state <= training;
						mode_changed <= '0';
					end if;
				when others =>
					state <= main_menu;
			end case;
		end if;
	end process nextState;
	
	catchCounting: process (v_sync, caught1, caught2) -- manage ball catches, level incrementing, etc.
	begin
		if rising_edge(v_sync) then
		case state is 
				when regular =>
					if caught1 = '1' or caught2 = '1' then -- if either ball is caught
						if catch_counter = 4 then -- if we're ready to move on
							catch_counter <= 0;
							v_level <= v_level + '1';
						else -- otherwise just record catches
							if (caught1 = '1' and caught2 = '1') then -- if both balls are caught simultaneously, count twice
								catch_counter <= catch_counter + 2;
							else -- just count once if only one ball is caught
								catch_counter <= catch_counter + 1;
							end if;
						end if;
					end if;
				when training =>
					if caught1 = '1' or caught2= '1' then -- if either ball is caught
						if catch_counter = 9 then -- if we're ready to move on
							catch_counter <= 0;
							v_level <= v_level + '1';
						else -- otherwise just record catches
							if (caught1 = '1' and caught2 = '1') then  -- if both balls are caught simultaneously, count twice
								catch_counter <= catch_counter + 2;
							else -- just count once if only one ball is caught
								catch_counter <= catch_counter + 1;
							end if;
						end if;
					end if;
				when others => 
					v_level <= "00";
					catch_counter <= 0;
			end case;
		end if;
	end process catchCounting;
	
	mode <= 	"00" when state = main_menu else
				"01" when state = regular else
				"10" when state = training;

	enable <= '0' when state = main_menu else
				 '1';
	
	level <= v_level;
end architecture;